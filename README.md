# omdb_api

implements an Rest API to an existing rest api of omdb site

implemented with:
Java 8
Spring server
Spring RestController
Spring Service (jackson xml converter) and Component (file cache)

with cache of files in C:/Temp/OMDB directory for movies searched by title or id

prerequirments:
- java jdk: https://www.java.com/en/download/manual.jsp
(and java jre which is included in the jdk)

make sure you have jdk on your computer:
-	open cmd
-	type: java -h (see output exists)
- 	type: javac (see output exists)
if not you need to install the jdk 8 to run this.

run:
-	open cmd
- 	cd to the directory you pulled the repo
-	type: gradlew bootRun (you compiled the files and started the tomcat server)
-	tomcat server will start listening on port 8080
-	open browser and try one of the api options


API URLS:
search api:
http://localhost:8080/weis/omdb/api/bysearch?s={searchName}
* s - for search is obligated for the request to work
option params for search:
s={search}
type={movie,series,episode}
y={releaseYear}
page={1-100}

by id or title:
http://localhost:8080/weis/omdb/api/byidortitle?t={title}$i={id}
* t & i - one of them is obligated for the byidortitle
* the movie cache is working only on the byidortitle - because of the diffrent structure of the search result and the byidortitle result
* after looking for a movie with the byidortitle api then you can look at C:/Temp/OMDB and see the movies cache
options params for id or title:
i={id}
t={title}
type={movie,series,episode}
y={yearRelease}
plot={short,full}

* look for attribute "isFromCache" in the xml result to see if the result is from the file system cache or not 

examples tests:
http://localhost:8080/weis/omdb/api/bysearch?s=coco&y=2017
http://localhost:8080/weis/omdb/api/byidortitle?t=coco



