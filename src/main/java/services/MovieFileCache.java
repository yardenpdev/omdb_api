package services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.HashBiMap;

/**
 * @author Yarden Peleg
 * component for movie file cache of the xml files
 * exposes get and add
 * the get is working for only id and only title
 */
@Component
public class MovieFileCache {
	
	private static HashBiMap<String, String> movieToIdMap;
	
	private static final String DIR_PATH = "C:/Temp/OMDB/";
	private static final String CACHE_FILE_NAME = "CACHE_MAP.bin";
	private static final String CACHE_FILE_PATH = DIR_PATH+"/"+CACHE_FILE_NAME;
	private static final String MOVIE_FILE_NAME_SUFIX = ".xml";
	
	public MovieFileCache() {
		new File(DIR_PATH).mkdirs();
		try {
			movieToIdMap = readCacheMap();
			if(movieToIdMap == null) {
				movieToIdMap = HashBiMap.<String, String>create();
				writeCacheMap();
			}
		}
		catch(Exception e) {
			movieToIdMap = null;
		}
		
	}
	
	private HashBiMap<String, String> getCacheMap() throws Exception {
		if(movieToIdMap == null) {
			movieToIdMap = readCacheMap();
		}
		if(movieToIdMap == null)
			throw new Exception("movie cache not initilized and not in file");
		return movieToIdMap;
	}
	
	/**
	 * gets the title form the id
	 * @param id
	 * @return
	 * @throws Exception
	 */
	private String getFromInverseMap(String id) throws Exception {
		return getCacheMap().inverse().get(id);
	}
	
	/**
	 * gets the xml string of the movie if the movie file exists
	 * get will get by the title or the id, it is possible to use both
	 * the cache will check first for the title if the title is not found then by id
	 * if not found by title or by id then null is returned
	 * @param title
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public synchronized String get(String title, String id) throws Exception {
		String localTitle = title;
		if(localTitle == null) {
			localTitle = getFromInverseMap(id);			
		}
		String xmlRes = readFile(getMoviePath(localTitle));
		return xmlRes;
	}
	
	private String getMoviePath(String title) {
		return DIR_PATH+"/"+getMoviefileName(title);
				
	}
	
	private String getMoviefileName(String title) {
		return title + MOVIE_FILE_NAME_SUFIX;
	}
	
	/**
	 * adds the title and id to represent the xml file
	 * the xml file is writen to file to the folder DIR_PATH
	 * @param title
	 * @param id
	 * @param xml
	 */
	public synchronized void add(String title, String id, String xml) {
		movieToIdMap.put(title, id);
		writeCacheMap();
		writeFile(getMoviePath(title), xml);
	}
	
	
	private HashBiMap<String, String> readCacheMap() {
		HashBiMap<String, String> map = null;
		try(ObjectInputStream oos = new ObjectInputStream(new FileInputStream(CACHE_FILE_PATH));) {
			Object obj = oos.readObject();
			if(obj instanceof HashBiMap)
				map = (HashBiMap<String, String>) obj;
			else
				throw new ClassNotFoundException();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		return map;
	}
	
	private void writeCacheMap() {
		writeObjectToFile(CACHE_FILE_PATH, (Serializable) movieToIdMap);
	}
	
	private void writeObjectToFile(String filePath, Serializable objSer) {		
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath))) {
			oos.writeObject(objSer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	private String readFile(String path) {
		StringBuilder sb = new StringBuilder();
		try(BufferedReader buf = new BufferedReader(new InputStreamReader(new FileInputStream(path)))) {
			String line = buf.readLine();
			while(line != null){ 
				sb.append(line).append("\n");
				line = buf.readLine();
			}
		}
		catch(IOException e) {
			e.printStackTrace();
			return null;
		}
		return sb.toString();
	}
	
	private void writeFile(String path, String toWrite) {
		File f= new File(path);
		try {
			FileUtils.writeStringToFile(f, toWrite);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

}
