package services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;

import models.OmdbResult;

/**
 * @author Yarden Peleg
 * service that allows conversion of objects to xml string
 */
@Service
public class JsonToXmlService {
	
     @Autowired
     private MappingJackson2XmlHttpMessageConverter xmlConverter;
	
	/**
	 * Receives a valid object and returns an xml string 
	 * representation of the object
	 * @param obj
	 * @return
	 * @throws JsonProcessingException
	 */
	public String convert(Object obj) throws JsonProcessingException {
		XmlMapper xmlMapper = (XmlMapper) xmlConverter.getObjectMapper();
		xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
		xmlMapper.configure( ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true );

	    return xmlMapper.writeValueAsString(obj);
	}
}
