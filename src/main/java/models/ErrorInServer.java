package models;

import java.io.Serializable;

/**
 * @author Yarden Peleg
 * 
 * model to represent server errors
 *
 */
public class ErrorInServer implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9218719081841503601L;
	private Exception e;
	private String message;
	
	public ErrorInServer(Exception e, String msg) {
		this.setE(e);
		this.setMessage(msg);
	}

	public Exception getE() {
		return e;
	}

	public void setE(Exception e) {
		this.e = e;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}



}
