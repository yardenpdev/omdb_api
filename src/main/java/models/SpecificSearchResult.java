package models;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Yarden Peleg
 * model to represent omdb results
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SpecificSearchResult extends OmdbSearchResult implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -203092998181646997L;

	@JsonProperty("Rated")
	private String rated;
	
	@JsonProperty("Released")
	private String released;
	
	@JsonProperty("Runtime")
	private String runtime;
	
	@JsonProperty("Director")
	private String director;
	
	@JsonProperty("Writer")
	private String writer;
	
	@JsonProperty("Actors")
	private String actors;
	
	@JsonProperty("Plot")
	private String plot;

	@JsonProperty("Language")
	private String language;
	
	@JsonProperty("Country")
	private String country;
	
	@JsonProperty("Awards")
	private String awards;
	
	@JsonProperty("Ratings")
	private List<RatingSearchResult> ratings;
	
	@JsonProperty("Metascore")
	private String metascore;
	
	@JsonProperty("imdbRating")
	private String imdbRating;
	
	@JsonProperty("imdbVotes")
	private String imdbVotes;
	
	@JsonProperty("DVD")
	private String dvd;
	
	@JsonProperty("BoxOffice")
	private String boxOffice;
	
	@JsonProperty("Production")
	private String production;
	
	@JsonProperty("Website")
	private String website;
	
	@JsonProperty("Response")
	private Boolean response;
	
	@JsonProperty("isFromCache")
	private Boolean isFromCache = false;

	public Boolean getIsFromCache() {
		return isFromCache;
	}

	public void setIsFromCache(Boolean isFromCache) {
		this.isFromCache = isFromCache;
	}
	
	public String getRated() {
		return rated;
	}

	public void setRated(String rated) {
		this.rated = rated;
	}

	public String getReleased() {
		return released;
	}

	public void setReleased(String released) {
		this.released = released;
	}

	public String getRuntime() {
		return runtime;
	}

	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getAwards() {
		return awards;
	}

	public void setAwards(String awards) {
		this.awards = awards;
	}

	public List<RatingSearchResult> getRatings() {
		return ratings;
	}

	public void setRatings(List<RatingSearchResult> ratings) {
		this.ratings = ratings;
	}

	public String getMetascore() {
		return metascore;
	}

	public void setMetascore(String metascore) {
		this.metascore = metascore;
	}

	public String getImdbRating() {
		return imdbRating;
	}

	public void setImdbRating(String imdbRating) {
		this.imdbRating = imdbRating;
	}


	public String getDvd() {
		return dvd;
	}

	public void setDvd(String dvd) {
		this.dvd = dvd;
	}

	public String getBoxOffice() {
		return boxOffice;
	}

	public void setBoxOffice(String boxOffice) {
		this.boxOffice = boxOffice;
	}

	public Boolean getResponse() {
		return response;
	}

	public void setResponse(Boolean response) {
		this.response = response;
	}

	public String getImdbVotes() {
		return imdbVotes;
	}

	public void setImdbVotes(String imdbVotes) {
		this.imdbVotes = imdbVotes;
	}
	
	
	@Override
	public String toString() {
    	StringBuilder sb = new StringBuilder();
    	sb.append("SpecificSearchResult").append(":").append("{");
    	sb.append("title").append(":").append(getTitle());
    	sb.append(",");
    	
    	sb.append("year").append(":").append(getYear());
    	sb.append(",");
    	sb.append("imdbID").append(":").append(getImdbID());
    	sb.append(",");
    	sb.append("type").append(":").append(getType());
    	sb.append(",");
    	sb.append("poster").append(":").append(getPoster());
    	sb.append(",");
    	
    	sb.append("rated").append(":").append(getRated());
    	sb.append(",");
    	sb.append("released").append(":").append(getReleased());
    	sb.append(",");
    	sb.append("runtime").append(":").append(getRuntime());
    	sb.append(",");
    	sb.append("director").append(":").append(getDirector());
    	sb.append(",");
    	sb.append("writer").append(":").append(getWriter());
    	sb.append(",");
    	sb.append("actors").append(":").append(getActors());
    	sb.append(",");
    	sb.append("plot").append(":").append(getPlot());
    	sb.append(",");
    	sb.append("language").append(":").append(getLanguage());
    	sb.append(",");
    	sb.append("country").append(":").append(getCountry());
    	sb.append(",");
    	sb.append("awards").append(":").append(getAwards());
    	sb.append(",");
    	sb.append("ratings:(");
    	for (Iterator<RatingSearchResult> iterator = ratings.iterator(); iterator.hasNext();) {
			RatingSearchResult ratingSearchResult = iterator.next();
			sb.append(ratingSearchResult.toString()).append(",");
		}
    	sb.append("),");
    	sb.append("metascore").append(":").append(getMetascore());
    	sb.append(",");
    	sb.append("imdbRating").append(":").append(getImdbRating());
    	sb.append(",");
    	
    	sb.append("imdbVotes").append(":").append(getImdbVotes());
    	sb.append(",");
    	sb.append("dvd").append(":").append(getDvd());
    	sb.append(",");
    	sb.append("boxOffice").append(":").append(getBoxOffice());
    	sb.append(",");
    	sb.append("production").append(":").append(getProduction());
    	sb.append(",");
    	sb.append("website").append(":").append(getWebsite());
    	
    	
    	sb.append("}");
    	return sb.toString();
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getPlot() {
		return plot;
	}

	public void setPlot(String plot) {
		this.plot = plot;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProduction() {
		return production;
	}

	public void setProduction(String production) {
		this.production = production;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

}
