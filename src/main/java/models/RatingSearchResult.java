package models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Yarden Peleg
 * model to represent omdb results
 */
public class RatingSearchResult {

	@JsonProperty("Source")
	private String source;
	
	@JsonProperty("Value")
	private String value;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
    	sb.append("source").append(":").append(getSource());
    	sb.append(",");
    	sb.append("value").append(":").append(getValue());
		return sb.toString();
	}
}
