package models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Yarden Peleg
 * model to represent omdb results
 */
public class OmdbSearchResult implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2123311494648676637L;
	
	@JsonProperty("Title")
    private String title;
	@JsonProperty("Year")
    private String year;
	@JsonProperty("imdbID")
    private String imdbID;
	@JsonProperty("Type")
    private String type;
	@JsonProperty("Poster")
    private String poster;


    @Override
    public String toString() {
    	StringBuilder sb = new StringBuilder();
    	sb.append("OmdbSearchResult").append(":").append("{");
    	sb.append("title").append(":").append(getTitle());
    	sb.append(",");
    	
    	sb.append("year").append(":").append(getYear());
    	sb.append(",");
    	sb.append("imdbID").append(":").append(getImdbID());
    	sb.append(",");
    	sb.append("type").append(":").append(getType());
    	sb.append(",");
    	sb.append("poster").append(":").append(getPoster());
    	  	

    	sb.append("}");
    	return sb.toString();
    }
    



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getYear() {
		return year;
	}



	public void setYear(String year) {
		this.year = year;
	}



	public String getImdbID() {
		return imdbID;
	}



	public void setImdbID(String imdbID) {
		this.imdbID = imdbID;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	public String getPoster() {
		return poster;
	}



	public void setPoster(String poster) {
		this.poster = poster;
	}



}
