package models;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Yarden Peleg
 * model to represent omdb results
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class OmdbResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8271962269025724165L;
	
	@JsonProperty("totalResults")
	private Integer totalResults;
	@JsonProperty("Response")
	private Boolean response;
	@JsonProperty("Search")
	private List<OmdbSearchResult> search;
	
	public Integer getTotalResults() {
		return totalResults;
	}
	public void setTotalResults(Integer totalResults) {
		this.totalResults = totalResults;
	}
	public Boolean getResponse() {
		return response;
	}
	public void setResponse(Boolean response) {
		this.response = response;
	}
	public List<OmdbSearchResult> getSearch() {
		return search;
	}
	public void setSearch(List<OmdbSearchResult> search) {
		this.search = search;
	}	
	
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("OmdbResult").append(":").append("{");
		sb.append("Search").append(":(");
		for (Iterator<OmdbSearchResult> iterator = getSearch().iterator(); iterator.hasNext();) {
			OmdbSearchResult omdbSearchResult = iterator.next();
			sb.append(omdbSearchResult.toString()).append(",");
			
		}
		sb.append("),");
		
		sb.append("totalResults").append(":").append(getTotalResults());
		sb.append(",");
		sb.append("Response").append(":").append(getResponse());
		sb.append("}");
		return sb.toString();
	}


}
