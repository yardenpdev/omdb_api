package controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;

import models.ErrorInServer;
import models.OmdbResult;
import models.SpecificSearchResult;
import services.JsonToXmlService;
import services.MovieFileCache;

/**
 * @author Yarden Peleg
 * controller that exposes 2 GET functionality for the movie/series/episoid 
 */
@RestController
public class OmdbController {
	
	@Autowired
	JsonToXmlService jsonToXmlService;
	
	@Autowired
	MovieFileCache movieFileCache;
	
	private final String URL_OMDB =  "http://www.omdbapi.com/";
	private final String API_KEY = "d9a1a503";
	
	// request parameters
	private final String API_KEY_NAME = "apikey";
	private final String API_KEY_URI_VAR = String.format("&apikey={%s}", API_KEY_NAME);
	private final String TITLE_NAME = "t";
	private final String URL_VAR_TITLE = String.format("&t={%s}", TITLE_NAME);
	private final String ID_NAME = "i";
	private final String URL_VAR_ID = String.format("&i={%s}", ID_NAME);
	private final String TYPE_NAME = "type";
	private final String URL_VAR_TYPE = String.format("&type={%s}", TYPE_NAME);
	private final String YEAR_NAME = "y";
	private final String URL_VAR_YEAR = String.format("&y={%s}", YEAR_NAME);
	private final String PLOT_NAME = "plot";
	private final String URL_VAR_PLOT = String.format("&plot={%s}", PLOT_NAME);
	private final String R_NAME = "r";
	private final String URL_VAR_R = String.format("&r={%s}", R_NAME);
	private final String SEARCH_NAME = "s";
	private final String URL_VAR_S = String.format("&s={%s}", SEARCH_NAME);
	private final String PAGE_NAME = "page";
	private final String URL_VAR_PAGE = String.format("&page={%s}", PAGE_NAME);
	
	private final String URL_OMDB_API_KEY_TEMP = URL_OMDB + "?" + API_KEY_URI_VAR;
	
	private final RestTemplate restToOmdb =  new RestTemplate();
	
	
	
	/**
	 * GET api for the bySearch api of the omdb api 
	 * url: localhost:8080/weis/ombd/api/byidortitle
	 * and the following optional params
	 * @param search search param
	 * @param type type param (movie, series, episode)
	 * @param year release year
	 * @param page amount of page to rerturn
	 * @return
	 */
	@RequestMapping(value = "/weis/omdb/api/bysearch", method = RequestMethod.GET, produces=MediaType.APPLICATION_XML_VALUE)
	public String getDataBySearch(
			@RequestParam(value=SEARCH_NAME, required=true) String search,
			@RequestParam(value=TYPE_NAME, required=false) String type,
			@RequestParam(value=YEAR_NAME, required=false) String year,
			@RequestParam(value=PAGE_NAME, required=false) String page) {
		String response = "";
		try {
			StringBuilder sbUrl = new StringBuilder(URL_OMDB_API_KEY_TEMP);
			Map<String, String> variables = new HashMap<>();
			variables.put(API_KEY_NAME, API_KEY);
			if(search != null) {
				variables.put(SEARCH_NAME, search);
				sbUrl.append(URL_VAR_S);
			}
			if(type != null) {
				variables.put(TYPE_NAME, type);
				sbUrl.append(URL_VAR_TYPE);
			}
			if(year != null) {
				variables.put(YEAR_NAME, year);
				sbUrl.append(URL_VAR_YEAR);
			}
			if(page != null) {
				variables.put(PAGE_NAME, page);
				sbUrl.append(URL_VAR_PAGE);
			}
			
			OmdbResult resObj = restToOmdb.getForObject(sbUrl.toString(), OmdbResult.class, variables);
			response = jsonToXmlService.convert(resObj);
			
		}
		catch(Exception e) {
			try {
				response = jsonToXmlService.convert(new ErrorInServer(e, e.getMessage()));
			} catch (JsonProcessingException e1) {
				response = e.toString();
			}
		}
		return response;
	}
	
	/**
	 * GET api for the byIdOrTitle api of the omdb api 
	 * localhost:8080/weis/ombd/api/byidortitle
	 * and the following optional params
	 * @param title title of the movie
	 * @param id imdbId of the movie
	 * @param type type param (movie, series, episode)
	 * @param year release year
	 * @param plot plot summery
	 * @return
	 */
	@RequestMapping(value = "/weis/omdb/api/byidortitle", method = RequestMethod.GET, produces=MediaType.APPLICATION_XML_VALUE)
	public  String getDataByIdOrTitle(			
			@RequestParam(value=TITLE_NAME, required=false) String title,
			@RequestParam(value=ID_NAME, required=false) String id,
			@RequestParam(value=TYPE_NAME, required=false) String type,
			@RequestParam(value=YEAR_NAME, required=false) String year,
			@RequestParam(value=PLOT_NAME, required=false) String plot) {
		String response;
		try {
			String res = movieFileCache.get(title, id);
			if(res != null) {
				return res;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("logger - cahce not working");
		}
	
		response = doGetByTitleOrId(title, id, type, year, plot);
		return response;
	}



	/**
	 * @param title title of the movie
	 * @param id imdbId of the movie
	 * @param type type param (movie, series, episode)
	 * @param year release year
	 * @param plot plot summery
	 * @return
	 */
	private String doGetByTitleOrId(String title, String id, String type, String year, String plot) {
		String response;
		SpecificSearchResult resObj;
		try {
			StringBuilder sbUrl = new StringBuilder(URL_OMDB_API_KEY_TEMP);
			Map<String, String> variables = new HashMap<>();
			variables.put(API_KEY_NAME, API_KEY);
			if(title != null) {
				variables.put(TITLE_NAME,title);
				sbUrl.append(URL_VAR_TITLE);
			}
			if(id != null) {
				variables.put(ID_NAME, id);
				sbUrl.append(URL_VAR_ID);
			}
			if(type != null) {
				variables.put(TYPE_NAME, id);
				sbUrl.append(URL_VAR_TYPE);
			}
			if(year != null) {
				variables.put(YEAR_NAME, year);
				sbUrl.append(URL_VAR_YEAR);
			}
			if(plot != null) {
				variables.put(PLOT_NAME, plot);
				sbUrl.append(URL_VAR_PLOT);
			}
			resObj = restToOmdb.getForObject(sbUrl.toString(), SpecificSearchResult.class, variables);
			response = jsonToXmlService.convert(resObj);
			if(resObj.getResponse() && resObj.getType().equals("movie")) {
				resObj.setIsFromCache(true);
				String cachedResponse = jsonToXmlService.convert(resObj);
				movieFileCache.add(resObj.getTitle(), resObj.getImdbID(), cachedResponse);
			}
		}
		catch(Exception e) {
			try {
				response = jsonToXmlService.convert(new ErrorInServer(e, e.getMessage()));
			} catch (JsonProcessingException e1) {
				response = e.toString();
			}
		}
		return response;
	}
}
